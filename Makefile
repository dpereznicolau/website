BLOG_ENTRIES := \
	20210606-tramposillu \
	20210314-7drl-2021 \
	20210303-hidden-online \
	20210226-mlrl-on-app-store \
	20201003-lotta-callbacks \
	20200926-annunciation-of-the-server \
	20200919-two-is-company-four-is-a-race \
	20200913-which-way-is-up \
	20200905-onflick-flick-flick \
	20200829-headless-gdx-and-tiled-maps-woes \
	20200823-flick-karts-a-new-beginning \
	$(NULL)

XML_ENTRIES := $(patsubst %,blog/%/index.xml,$(BLOG_ENTRIES))
HTML_ENTRIES := $(patsubst %.xml,%.html,$(XML_ENTRIES))

POST_XSL := blog/post.xsl
INDEX_XSL := blog/index.xsl

DOCROOT = /srv/www/htdocs/www.peritasoft.com


blog/index.html: blog/index.xml $(INDEX_XSL)
	xsltproc --output $@ $(INDEX_XSL) $<

.INTERMEDIATE: blog/index.xml

blog/index.xml: $(HTML_ENTRIES)
	echo '<posts>' > $@
	for f in $(BLOG_ENTRIES); do \
		printf '<post src="%s/index.xml" href="%s/index.html"/>\n' $$f $$f >> $@; \
	done
	echo '</posts>' >> $@

blog/%/index.html: blog/%/index.xml $(POST_XSL)
	xsltproc --output $@ $(POST_XSL) $<
	sed -i -e 's#</source>##g' $@

.PHONY: install

install: blog/index.html
	install -d $(DESTDIR)$(DOCROOT)
	install -m 644 \
		favicon.ico \
		index.html \
		privacy.html \
		perita.gif \
		$(DESTDIR)$(DOCROOT)
	install -d $(DESTDIR)$(DOCROOT)/css
	install -m 644 css/*.css $(DESTDIR)$(DOCROOT)/css
	install -d $(DESTDIR)$(DOCROOT)/img
	install -m 664 img/* $(DESTDIR)$(DOCROOT)/img
	install -d $(DESTDIR)$(DOCROOT)/blog
	install -m 664 \
		blog/cc-by-sa-4.0-88x31.png \
		blog/index.html \
		blog/perita.png \
		blog/screen.css \
		$(DESTDIR)$(DOCROOT)/blog
	for f in $(BLOG_ENTRIES); do \
		install -d $(DESTDIR)$(DOCROOT)/blog/$$f ; \
		find blog/$$f -type f \! -name index.xml -print0 | xargs -0 install -m 644 -t $(DESTDIR)$(DOCROOT)/blog/$$f ; \
	done

.PHONY: clean

clean:
	$(RM) $(HTML_ENTRIES) blog/index.xml blog/index.html
