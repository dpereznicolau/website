<?xml version="1.0" encoding="UTF-8"?>
<article description="We find out that libGDX’s TmxMapLoader fails with a null pointer exception using the headless backend and explain a solution.">

<header>
	<h1 class="p-name">Headless <span class="h-product p-name">libGDX</span> and <span class="h-product p-name">Tiled</span> Maps Woes</h1>
	<p><time class="dt-published" datetime="2020-08-29">29 August 2020</time></p>
</header>

<p>
We have reached a point in <a class="h-product p-name" href="https://www.peritasoft.com/flickkarts/">Flick Karts</a> that has everything we need to test the gameplay:
there is a placeholder circuit with barriers that we can zoom and pan,
multiple pucks that can be moved and collide with each other,
and the physics works well enough at this stage.
</p>

<figure style="max-width: 400px">
	<video controls="controls">
		<source src="placeholder-graphics.webm" type="video/webm"/>
	</video>
	<figcaption>Pan, zoom, and flicking a placeholder puck.</figcaption>
</figure>

<p>
It is now time to make it a multiplayer game.
</p>

<p>
We decided early on that this will be a turn-based multiplayer game:
players take turns, in a round-robin fashion, to either move their puck or use an item until all players have completed all laps.
One of the (many) errors i did with <a class="h-product p-name" href="https://www.peritasoft.com/bbb/">Boat Boom Boom</a> was to wait too much before attempting to wedge in, as it were, network features, even though we decided from the start that it would be a multiplayer game.
For some reason i though that, merely by keeping the logic and the rendering in separate packages, adding a multiplayer layer later on would not be “that hard”, and we concentrated on having everthing in place first.
Of course, i found out that network code is not plug-and-play and had to refactor most of the gameplay classes.
Even worse, i picked <a class="h-product p-name" href="https://github.com/EsotericSoftware/kryonet">Kryonet</a> as the networking library without realising that was not usable from GWT.
Although, to be honest, we did not plan to make an HTML5 version of <span class="h-product p-name">Boat Boom Boom</span> until after we built <a class="h-product p-name" href="https://www.peritasoft.com/mlrl/">My Little Roguelike</a>.
</p>

<p>
We did not want to make the same mistakes this time and agreed to add the networking as early as was feasible.
Also, the server will be based on websockets, even for Android and Destkop, so that the HTML5 client can connect to it.
</p>

<p>
Today we began writing what will hopefully become the server and it uses the <a href="https://github.com/libgdx/libgdx/tree/5061b0c5a797cb735d5e33fec39ec6a9a6bd83f2/backends/gdx-backend-headless">headless backend of <span class="h-product p-name">libGDX</span></a>.
It is mostly the same as the desktop backend but does not initialize the video and sound subsystems, and has only a console;
ideal for servers applications.
</p>

<p>
The only caveat in not having a graphics subsystem is that you can not create textures:
it throws a <code>NullPointerException</code> when it tries to use the OpenGL context to generate the texture.
This is usually not a problem because it is not necessary, nor useful, to load textures in a standalone server application and we can just skip that step, but we had an issue when loading <a href="https://www.mapeditor.org/" class="h-product p-name">Tiled</a> maps.
</p>

<p>
The placeholder circuit was made using <span class="h-product p-name">Tiled</span> with two layers, one layer with the position of the tiles and the second with polyline objects that define the collision shapes, that is the circuit’s barriers.
The client needs both layers, because it renders the circuit on screen and uses the collision data to perform the same physics simulation as the server and anticipate all movements, while the server only needs to load the collision layer.
However, we were unable to find a way to load only the object layer using <span class="h-product p-name">libGDX</span>’s <code><a href="https://libgdx.badlogicgames.com/ci/nightlies/docs/api/com/badlogic/gdx/maps/tiled/TmxMapLoader.html">TmxMapLoader</a></code>;
it will always load the tileset layer and create the required texture for the tiles, therefore emiting a <code>NullPointerException</code> every time.
</p>

<p>
At the end we had to create a new class, say <code>ServerMapLoader</code>, that overides the <code>load</code> method in a way that prevents the creation of textures.
Please note that it still requires that the <code><a href="https://libgdx.badlogicgames.com/ci/nightlies/docs/api/com/badlogic/gdx/maps/ImageResolver.html">ImageResolver</a></code> returns a non-null <code>TextureRegion</code>, even though it is strictly speaking not necessary.
</p>

<figure>
<pre><code>
public class ServerMapLoader extends TmxMapLoader {
    @Override
    public TiledMap load(String fileName, Parameters parameter) {
        FileHandle tmxFile = resolve(fileName);
        this.root = xml.parse(tmxFile);

        TiledMap map = loadTiledMap(tmxFile, parameter, new ImageResolver() {
            @Override
            public TextureRegion getImage(String name) {
                return new TextureRegion();
            }
        });
        return map;
    }
}
</code></pre>
</figure>

</article>
